# Salebarn Accelerated Negotiation

A repository of annoated curated reading lists and methods and data formats to aid the process of annotating and curating these lists for HUMAN learning, not machine learning -- although machine learning is something that we will learn about.

Our focus is not upon automation or using machines to robotically created data APIs ... rather our focus is upon the annotation and writing about what is in the list ... first a human must READ, IMPLEMENT, REVISE and think about collected dots of information AND then a human must articulate the connections between the dots for other humans.  

This repository represents and an ongoing exploration of open source technologies, data APIS and machine learning operations ... for purposes such as direct marketing of produce,accelerated negotiation technologies, improvment in auction reliabilty and quality of execution, trading exchanges and platforms, secure and reliable financial technologies, cryptocurrencies and loyalty-reward programs, sales and CRM tools, marketing and PR campaigns, advertising and effectiveness measurement, recommendation engines and AI/ML ... and more.
