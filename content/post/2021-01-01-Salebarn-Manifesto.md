---
title: Salebarn Manifesto
subtitle: Revisiting the Cluetrain Manifesto
date: 2021-01-01
tags: ["ReadingList", "intelligence", "community"]
---

Go back and look at the Cluetrain Manifesto again ... review it closely ... then review it again ... realize why you still haven't reviewed it enough. **Markets are conversations** 

GENUINE conversations are collaborative.  They are about understanding at deep level ... NOT for a debate stage ... but understanding FOR living liffe ... so they are necessarily about teasing, bantering, bickering, arguing, clarifying, affirming, tearing down, supporting.  Genuine conversations are like an auction where negotiation REALLY matters because money is going to change hands and somebody, but not everybody is going to take something home. 

*GENUINE conversations go BEYOND nice chatter about distribution of the participation ribbons and Something We ALL Get ... GENUINE conversations are give-and-take, back-and-forth bartering and negotiation and reaching consensus on what matters, how much it matters, who pays, who plays and who watches.*

***Markets are dead serious, life-affecting, crucially more important ADULT conversations.*** *The corollary is that most people of adult-age are not simple not grown-up enough yet to understand the depth of negotiations and information given off by those market negotiations.*

The Cluetrain Manifesto was put together over two decades ago ... went live in April 1999 ... *A powerful global conversation has begun. Through the Internet, people are discovering and inventing new ways to share relevant knowledge with blinding speed. As a direct result, markets are getting smarter, ...and getting smarter faster than most companies.* 

**Markets are conversations** When these conversations get serious, down to brass tacks, they turn into negotiatons.** 

# Accelerate the negotiation.

Understand the CLOSE of the conversation. Make it easier for people to reliably find and appraise information to compare, contrast, determine value.

Develop better ways to involve the interested parties in exploring the offering ... is the object something that they cannot live without ... help them understand their WHY ... it's important for people to understand their UNIQUE individual WHY ... there is no long-term sustainable gain from someone pursuing something that they merely temporarily WANT. What is their WHY ... what are they willing to part with to have something they NEED?

# Inbound marketing is a conversation about culture.

We seem to have stumbled into a ditch of a culture sewer when it comes to the emphasis on being like.  Being liked has NEVER EVER EVER really helped anyone ... if you want to help someone, give them information that radically changes their worldview, ie something that they can act on right now ... stop regurgitating the same old palatable shit they hear elsewhere and stop buying alcholics drinks or getting the addicts high. Affluence and comfort are strictly impediments to a healthy culture ... discipline equal freedom; help people build their own discipline ... give them actionable, possibly painful insights.

Culture matters immensely; it is the emotional or psychological house that we live in and need for shelter and comfort ... culture is the house that we trust and generally take for granted, never really think about until something is not right.

Markets should be about building a culture of TRUE value ... unfortunately, markets are all too often about only signalling value or what people imagine is value. It is the responsibility of people engaged in marketing to help people build their own culture of their own true value.

MOST people fail to understand that they have exactly ONE life ... and their life will be under their control AFTER they understand that they should not worry about how their life looks to anyone else ... it is THEIR life and ONLY their life ... people who are stupid or evil enough to want other things for a person who truly understands their why need to be silent and disappear from the conversation. Individuals must KNOW their why ... marketers can help accelerate the internal negotiation or debate, they can add a sense of urgency, but should not make offers or bids or commitments on behalf of other parties.

We see in this in the world of fundraising ... most fundraising is about raising funds to pay for the fundraising that will bring in the next round of fundraising. Perpetual fundraising for the sake of fundraising is parasitism and BROKEN.  ALL shrewd people with anything approaching an impactful stock of funds that the fundraising parasites are after should already know this ... they should understand why only IDIOTS write checks .... actually DOING something that makes a difference requires direct involvement.

If you want to help anyone ... you must help them to obtain or build their tools and processes to help themselves by getting their hands on the information that changes their worldview ... you cannot change their worldview for them or merely give them the cold shower they need, but more importantly, you cannot make them better off with false assurances that reinforce their blindness or help them to pretend all's well, their concerns are caused by outsiders or aliens -- they, themselves are manufacturing the difficulties which produce the circumstances which manifest themselves as a hint of a concern, eg people in poor health are justifiably batshit worried about something like covid, not because of covid, but because they know that their health is extra fragile ... stress and pain are positives when they are rooted in Reality; these things shouldn't be medicated or numbed ... buying alcoholics drinks does not help alcoholics, getting an alcholic to get to the point where he realizes that he is powerless over alcohol and that the alcohol addles his brain and makes all of his decisions thereafter wrong and makes him progressively worse off ... if you want to help someone, you cannot sober them up, you can assist in getting them to the point where they realize for themselves why they must sober up.

This is why, although capitalism is not yet entirely dead, but it has gotten really old and feeble, ie many think that govt programs create opportunities ... and many in politics are focused on glory days of four or five decades ago and have a completely demented about what worked and why. So most business models that we find in capitalism really belong in the old folks home, ie they worked fantastic in the 1960s but are not appropriate for today's environment and the way business is done in the virtual realm ... they might work to a degree, in terms of providing comfort to patients that are circling the drain but they are not going to change how business is done going forward. 

Marketing that genuinely accelerates the development of genuine insight is what necessary for helping people to overcome the feebleness of not being in control of their own destinies ... it will always be true that people need to focus most of their efforts on what they can control -- what changes is in the addition of actionable intelligence ... how does the extra information result in smarter choices, more resolute action or a sustainable improvement in productivity/health which opens up new opportunities in the future, ie you don't have opportunities when you are decaying, sick or dead.

BIG ideas and the kinds of things that truly change things don't require that much in the way of seed capital ... Linux, Git, shared intellectual property and open source communities are an example -- even though the notion [held by some] that software licenses can be structured to limit use to only approved uses is categorically both evil and especially stupid ... really big ideas make their own music and rapidly attract communities of extra creative, ridiculously talented developers who ALL think different from one another ... EVIL is found in zombie herds of like-minded gregarious egalitarian bovines.

Actionable intelligence is the new currency ... knowing what to do to get the problem to stop from recurring is more valuable than drugs, distractions and comfortable chairs/deathbeds. Namespaces and ways to get past dependency hell were a big deal in the late 90s, but it took until the 2010s before people understood the intell on what became known as a container ... or the ways of reliably using containers to rapidly spin up or instantiate machine images with application stacks running in containers.

If you really want to accelerate the negotiation now ... develop a radically SIMPLER way to accomplish security ... make it easier for individuals to SECURELY join crowds and gather information from that crowd to compare, contrast, determine value ... it has to more than just involving the interested parties in exploring the offering ... they have to be able to participate without entirely showing their hand ... they might care about some object, asset or something that they cannot live without ... help them understand their WHY; the uniqueness of that WHY is also the root of security ... it's important for people to work on understanding that WHY ... there is no long-term sustainable gain from someone pursuing something that they merely temporarily WANT. What is their WHY ... what are they willing to part with to have something they NEED?  The introspection of that WHY is has to be their Will, their plan for their Life, the key to their wallet ... anyone violating the boundaries of that private, unique information must die ... curiosity in those things must result in the death of that cat.


